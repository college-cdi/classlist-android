package com.cdi.classList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    data class Class(val Id: String, val Name: String, val Duration: Int);

    class ClassAdapter(val classes: ArrayList<Class>): RecyclerView.Adapter<ClassAdapter.ViewHolder>() {

        // Holds the views for adding it to image and text
        class ViewHolder(classView: View) : RecyclerView.ViewHolder(classView) {
            val classId: TextView = classView.findViewById(R.id.classId)
            val className: TextView = classView.findViewById(R.id.className)
            val classDuration: TextView = classView.findViewById(R.id.classDuration)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent.context);
            val view = inflater.inflate(R.layout.class_component, parent, false)
            return ViewHolder(view);
        }

        override fun getItemCount(): Int {
            return classes.size;
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item: Class = classes[position];

            // sets the image to the imageview from our itemHolder class
            holder.classId.text = item.Id;
            holder.className.text = item.Name;
            holder.classDuration.text = item.Duration.toString();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView: RecyclerView = findViewById(R.id.classView)
        val classes: ArrayList<Class> = arrayListOf<Class>(Class("420-PPA-ID", "Profession Programmeur-Analyste", 60),
            Class("420-ARP-ID", "Approche Structurée à la résolution de problèmes", 60),
            Class("420-PO1-ID","Programation Orienté Objet 1", 60),
            Class("420-PO2-ID","Programmation Orienté Objet 2", 60),
            Class("420-DM1-ID", "Développement Mobile 1", 60),
            Class("420-DM2-ID", "Développement Mobile 2", 60),
            Class("420-1NF-ID", "Info-Nuagique", 60),
            Class("420-AWB-ID", "Animation Web", 60),
            Class("420-BD1-ID", "Base de Données 1", 60),
            Class("420-BD2-ID", "Base de Données 2", 60),
            Class("420-DCS-ID", "Développement Web coté Serveur", 60),
            Class("420-DW1-ID", "Développement Web 1", 60),
            Class("420-DW2-ID", "Développement Web 2", 60),
            Class("420-PWB-ID", "Programmation Web", 60),
            Class("420-TDD-ID", "Traitement De Données", 60)
            );

        recyclerView.adapter = ClassAdapter(classes);
        recyclerView.layoutManager = LinearLayoutManager(this);
    }
}